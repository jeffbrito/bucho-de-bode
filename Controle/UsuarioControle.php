<?php

require_once("../Modelo/Cliente.php");
require_once("Conexao.php");

class UsuarioControle{

	function InserirCliente($Cliente){
		try{
			$conexao = new Conexao();
			$nome = $Cliente->getNome();
			$email = $Cliente->getEmail();
			$telefone = $Cliente->getTelefone();
			$senha = $Cliente->getSenha();
			$sql = $conexao->getConexao()->prepare("INSERT INTO user(nome,senha,email,telefone) VALUES(:n,:s,:e,:t);");
			$sql->bindParam("n",$nome);
			$sql->bindParam("s",$senha);
			$sql->bindParam("e",$email);
			$sql->bindParam("t",$telefone);
			if($sql->execute()){
				$conexao->fecharConexao();
				return true;
			}else{
				$conexao->fecharConexao();
				return false;
			}
		}catch(PDOException $e){
			echo "Erro de PDO:{$e->getMessage()}";
			return false;
		}catch(Exception $e){
			echo "Erro geral:{$e->getMessage()}";
			return false;
		}

	}


	function UpdateCliente($id,$Cliente){
		try{
			$conexao = new Conexao();
			$nome = $Cliente->getNome();
			$email = $Cliente->getEmail();
			$telefone = $Cliente->getTelefone();
			$senha = $Cliente->getSenha();
			$sql = $conexao->getConenexao()->prepare("UPDATE user SET nome = :n, senha = :s, email = :e, telefone = :t WHERE id = :id");
			$sql->bindParam("id",$id);
			$sql->bindParam("n",$nome);
			$sql->bindParam("s",$senha);
			$sql->bindParam("e",$email);
			$sql->bindParam("t",$telefone);
			if($sql->execute()){
				$conexao->fecharConexao();
				return true;
			}else{
				$conexao->fecharConexao();
				return false;
			}
		}catch(PDOException $e){
			echo "Erro de PDO:{$e->getMessage()}";
			return false;
		}catch(Exception $e){
			echo "Erro geral:{$e->getMessage()}";
			return false;
		}

	}

	function removerCliente($id){
		try{
			$conexao = new Conexao();
			$cmd = $conexao->getConexao()->prepare("DELETE FROM admin WHERE id=:id");
			$cmd->bindParam("id",$id);
			if($cmd->execute()){
			    $conexao->fecharConexao();
			    return true;
			   
			}else{
			    $conexao->fecharConexao();
			    return false;
			}	
		}catch(PDOException $e){
			echo "Erro de pdo:{$e->getMessage()}";
		}catch(Exception $e){
			echo "Erro geral:{$e->getMessage()}";
		}
	

	}


	function selectIdnu($n,$s){
	    try{
	        $conexao = new Conexao();
	        $cmd = $conexao->getConexao()->prepare("SELECT id FROM user WHERE nome = :n AND senha = :s;");
	        $cmd->bindParam("n",$n);
	        $cmd->bindParam("s",$s);
	        $cmd->execute();
	        $result = $cmd->fetchAll(PDO::FETCH_CLASS,"Cliente");
	        return $result;
	    }catch(PDOException $e){
	        echo "Erro no pdo:{$e->getMessage()}";
	    }catch(Exception $e){
	        echo "Erro geral:{$e->getMessage()}";
	    }
	    
	    
	}

	function selectUeS($n,$s){
	    try{
	        $conexao = new Conexao();
	        $cmd = $conexao->getConexao()->prepare("SELECT * FROM user WHERE nome = :n AND senha = :s;");
	        $cmd->bindParam("n",$n);
	        $cmd->bindParam("s",$s);
	        if($cmd->execute()){
	            if($cmd->rowCount() == 1){
	                return true;
	            }else{
	                return false;
	            }
	        }
	    }catch(PDOException $e){
	        echo "Erro em pdo:{$e->getMessage()}";
	    }catch(Exception $e){
	        echo "Erro geral:{$e->getMessage()}";
	    }
	    
	}


}


?>