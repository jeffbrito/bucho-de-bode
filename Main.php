<?php
echo "<link rel='stylesheet' type='text/css' href='Visual/styletudo.css'>";
echo "<link type='text/css' rel='stylesheet' href='Visual/css/materialize.css'  media='screen,projection'/>";
echo "<link type='text/css' rel='stylesheet' href='Visual/css/custom.css' />";
echo "<meta name='viewport' content='width=device-width, initial-scale=1.0'/>";
echo "<script type='text/javascript' src='Visual/js/parallax.js'></script> ";
echo "  <script type='text/javascript' src='Visual/js/jquery.js'/></script>";
echo "  <link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>";

$dia = date('w');# Método para identificar qual é o dia da semana

#Condicão em conjunto com o método da semana, funciona como um array
#domingo = 0, segunda = 1 , e assim por diante.

echo "
				<nav class='navbar-fixed'>
           <a href='#' data-target='slide-out' class='sidenav-trigger hide-on-large-only'><i class='material-icons'>menu</i></a>

  				<div class='nav-wrapper'>
    					<a href='Main.php' class='brand-logo hide-on-med-and-down'>Bucho de Bode</a>
    					<ul id='nav-mobile' class='right hide-on-med-and-down'>
    					  <li><a href='Visual/cardapio.php'>Cardapio</a></li>
    					  <li><a href='Visual/Reservas.php'>Reservas</a></li>
    					    <a class='dropdown-trigger btn' href='#' data-target='dropdown1'>Opções</a>

                <ul id='dropdown1' class='dropdown-content'>
                    <li><a href='#'>Ajustar conta</a></li>
                    <li><a href='#'>Logout</a></li>
                    <li><a href='Visual/userDados.php'>Dados de usuário</a></li>
                </ul>
    					  <li><a href='Visual/sobreBode.php'>Sobre</a></li>
   				 </ul>
 				 </div>
				</nav>
       <ul id='slide-out' class='sidenav'>
        <li><div class='user-view'>
          <div class='background'>
            <img class='responsive-img' src='Visual/imagens/bannerT.svg' />
          </div>
            <a href=''><img class='circle responsive-img' alt='sem imagem escolhida'></a>
            <a href=''><span class='white-text name'>Individuo não logado</span></a>
            <a href=''><span class='white-text email'>emailncadastradado@gmail.com</span></a>
        </div></li>
          <li><a href='index.php' class='brand-logo flow-text'>Bucho de Bode</a></li>
                <li><div class='divider'></div></li>
                <li><a href='Visual/cardapio.php'>Cardapio</a></li>
                <li><a href='Visual/Reservas.php'>Reservas</a></li>
                    <a class='dropdown-trigger btn' href='#' data-target='dropdown2'>Opções</a>
                <li><a href='Visual/sobreBode.php'>Sobre</a></li>
                <ul id='dropdown2' class='dropdown-content'>
                    <li><a href='#'>Ajustar conta</a></li>
                    <li><a href='#'>Logout</a></li>
                    <li><a href='Visual/userDados.php'>Dados de usuário</a></li>
                </ul>
               
       </ul>



       ";
if ($dia > 0 && $dia <= 3) {
	echo "
<!-- Cardápio e funcionamento do parallax -->


      <div class='parallax-container'><!--container essêncial para o funcionamento do parallax, assim como a classe chamada na próxima div -->
            <div class='parallax'>
              <img class='responsive-img' src='Visual/imagens/bannerT.svg' />
            </div>
      </div>

      <div class='row container'>
				    <h1>As melhores comidas você encontra aqui !</h1>
 				 <p class='flow-text'>E sempre com o menor preço.</p>
      </div>

      <div class='parallax-container'>

 		   <div class='parallax'>
            <img src='Visual/imagens/pizza.jpeg' class='responsive-image'>
        </div>
      </div>



 				 <div class='container'>
 				 <h1>Promoção do Dia !</h1>
 				 <p class='flow-text'>cada dia da semana tem um prato diferente</p>
 				 </div>



";
}

if ($dia == 0) {
  include_once("Visual/day0.php");
}

if ($dia == 1) {
  include_once("Visual/day1.php");
}

if ($dia == 2) {
  include_once("Visual/day2.php");

}

if ($dia == 3) {
  include_once("Visual/day3.php");
}

if ($dia == 4) {
	include_once("Visual/day4.php");
}

if ($dia == 5) {
	include_once("Visual/day5.php");
}

if ($dia == 6) {
  include_once("Visual/day6.php");
}

echo "
<footer id='rodape' class='page-footer'>
          <div class='container'>
            <div class='row'>
              <div class='col l6 s12'>
                <h5 class='white-text'>Bucho de Bode</h3>
                <p class='grey-text text-lighten-4'>Restaurante</p>
              </div>
              <div class='col l4 offset-l2 s12'>
                <h5 class='white-text'>Siga-nos:</h5>
               <ul>
                  <li><a class='grey-text text-lighten-3' href=''>Instagram</a></li>
                  <li><a class='grey-text text-lighten-3' href=''>Twitter</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class='footer-copyright'>

            <div class='container'>
            © 2020 Copyright Bucho de Bode site.
            </div>
          </div>
  </footer>

  <script  src='Visual/js/materialize.js'></script>
  <!-- É obrigatório chamar o script depois do código -->
  <script>

    $(document).ready(function(){
    $('.sidenav').sidenav();
  });

  </script>
  <script>
        $(document).ready(function(){
    $('.parallax').parallax();
  });
  </script>

  <script>
      $('.dropdown-trigger').dropdown();
  </script>



";
