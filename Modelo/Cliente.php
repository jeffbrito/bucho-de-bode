<?php
class Cliente{
	private $id;
	private $nome;
	private $telefone;
	private $email;
	private $senha;
	//getters
		public function getId(){
			return $this->id;
		}
		public function getNome(){
			return $this->nome;
		}
		public function getTelefone(){
			return $this->telefone;
		}
		public function getEmail(){
			return $this->email;
		}
		public function getSenha(){
			return $this->senha;
		}
	//setters
		public function setId($i){
			$this->id = $i;
		}
		public function setNome($n){
			$this->nome = $n;
		}
		public function setTelefone($t){
			$this->telefone = $t;
		}
		public function setEmail($e){
			$this->email = $e;
		}
		public function setSenha($s){
			$this->senha = $s;
		}
}
?>
