<?php
	echo "<link rel='stylesheet' type='text/css' href='styleLogin.css'>";
	echo "<link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>";
	echo "<link type='text/css' rel='stylesheet' href='css/materialize.min.css'  media='screen,projection'/>";
	echo "<meta name='viewport' content='width=device-width, initial-scale=1.0'/>";
	echo "<script type='text/javascript' src='js/materialize.min.js'></script>";
	echo "
		<div class='navbar-fixed'>
    <nav>
      <div class='nav-wrapper'>
        <a href='pagFut.php' class='brand-logo'>Fut na Net</a>
        <ul class='right hide-on-med-and-down'>
          <li><a href='sobreFut.php'>Sobre</a></li>
        </ul>
      </div>
    </nav>
  </div>
    <img src='banner.jpg' class='responsive-img' id='banner'>
    <div class='container'>
      <table>
        <thead>
          <tr>
              <th><h4 class='header'>Veja o Historico de seus jogadores preferidos !</h4></th>
          </tr>
        </thead>

        <tbody>
          <tr>
          <th>
          <div class='col s12 m7'>
            <h5 class='header'>Neymar</h5>
          <div class='card horizontal'>
             <div class='card-image'>
               <img src='neymar.jpg'  id='neymar'>
             </div>
            <div class='card-stacked'>
               <div class='card-content'>
                 <p>
                    <h5>Neymar da Silva Santos Júnior, mais conhecido como Neymar Jr. ou apenas Neymar, é um futebolista brasileiro que atua como ponta-esquerda. Atualmente joga pelo Paris Saint-Germain e pela Seleção Brasileira. Revelado pelo Santos, em 2009, Neymar se tornou o principal futebolista em atividade no país.</h5></p>
               </div>
               <div class='card-action'>
                 <a class='light-green-text' href='neymarFut.php'><h6 class='right-align'>Ver Jogador</h6></a>
               </div>
            </div>
            </div>
          </div>
          </th>
          </tr>
          <tr>
            <th>
          <div class='col s12 m7'>
            <h5 class='header'>Ronaldinho Gaucho</h5>
          <div class='card horizontal'>
             <div class='card-image'>
               <img src='ronaldin.jpg'  id='roro'>
             </div>
            <div class='card-stacked'>
               <div class='card-content'>
                 <p>
                    <h5>
                        Ronaldo de Assis Moreira, mais conhecido como Ronaldinho Gaúcho ou simplesmente Ronaldinho, é um ex-futebolista brasileiro que atuava como meia-atacante. Atualmente é embaixador do Barcelona, clube em que fez história.</h5></p>
               </div>
               <div class='card-action'>
                 <a class='light-green-text' href='ronaldinFut.php'><h6 class='right-align'>Ver Jogador</h6></a>
               </div>
            </div>
            </div>
          </div>
          </th>
          </tr>
          </tr>
        </tbody>
      </table>


    </div>

	<br>
	<br>
	<footer id='rodape' class='page-footer'>
          <div class='container'>
            <div class='row'>
              <div class='col l6 s12'>
                <h5 class='white-text'>Fut na Net !</h5>
                <p class='grey-text text-lighten-4'>Só aqui você encontra os melhores !</p>
              </div>
              <div class='col l4 offset-l2 s12'>
                <h5 class='white-text'>Siga-nos:</h5>
                <ul>
                  <li><a class='grey-text text-lighten-3' href='https://www.instagram.com/deccatezu/?hl=pt-br'>Instagram Caio</a></li>
                  <li><a class='grey-text text-lighten-3' href='https://www.instagram.com/ld_lima_17/?hl=pt-br'>Instagram Davi</a></li>
                  <li><a class='grey-text text-lighten-3'>Whatsapp Caio:991072554</a></li>
                  <li><a class='grey-text text-lighten-3'>Whatsapp Davi:985487907</a></li>
                  <li><td colspan='3'><a href='pagLogin.php'><button>Sair</button></a></td>
                </ul>
              </div>
            </div>
          </div>
          <div class='footer-copyright'>
            <div class='container'>
            © 2019 Copyright Fut na net
            </div>
          </div>
        </footer>
	";
?>
