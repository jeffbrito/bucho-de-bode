<?php
echo "<link rel='stylesheet' type='text/css' href='styletudo.css'>";
echo "<link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet'>";
echo "<link type='text/css' rel='stylesheet' href='css/materialize.css'  media='screen,projection'/>";
echo "<meta name='viewport' content='width=device-width, initial-scale=1.0'/>";
echo "  <script type='text/javascript' src='js/jquery.js'/></script>";

echo "
        <nav class='navbar-fixed'>
           <a href='#' data-target='slide-out' class='sidenav-trigger hide-on-large-only'><i class='material-icons'>menu</i></a>

          <div class='nav-wrapper'>
              <a href='../index.php' class='brand-logo hide-on-med-and-down'>Bucho de Bode</a>
              <ul id='nav-mobile' class='right hide-on-med-and-down'>
                <li><a href='cardapio.php'>Cardapio</a></li>
                <li><a href='Reservas.php'>Reservas</a></li>
                    <a class='dropdown-trigger btn' href='#' data-target='dropdown1'>Opções</a>

                <ul id='dropdown1' class='dropdown-content'>
                    <li><a href='#'>Ajustar conta</a></li>
                    <li><a href='#'>Logout</a></li>
                    <li><a href='#'>Dados de usuário</a></li>
                </ul>
                <li><a href='sobreBode.php'>Sobre</a></li>
           </ul>
         </div>
        </nav>
       <ul id='slide-out' class='sidenav'>
        <li><div class='user-view'>
          <div class='background'>
            <img src='imagens/banner.jpeg' />
          </div>
            <a href=''><img class='circle responsive-img' alt='sem imagem escolhida'></a>
            <a href=''><span class='white-text name'>Individuo não logado</span></a>
            <a href=''><span class='white-text email'>emailncadastradado@gmail.com</span></a>
        </div></li>
          <li><a href='../index.php' class='brand-logo flow-text'>Bucho de Bode</a></li>
                <li><div class='divider'></div></li>
                <li><a href='cardapio.php'>Cardapio</a></li>
                <li><a href='Reservas.php'>Reservas</a></li>
                <li><a href='cadastroBode.php'>Login</a></li>
                    <li><a href='cadastroBode2.php'>Cadastro</a></li>
                <li><a href='sobreBode.php'>Sobre</a></li>
       </ul>
   <div class='container'>
   <div class='row'>
    <div class='col s12 m7'>
      <div class='card'>
        <div class='card-image'>
          <img src=''>
          <span class='card-title'>Sobre nos</span>
        </div>
        <div class='card-content'>
          <p></p>
        </div>
        <div class='card-action'>
        </div>
      </div>
    </div>
  </div>
   </div>

    <footer id='rodape' class='page-footer'>
          <div class='container'>
            <div class='row'>
              <div class='col l6 s12'>
                <h5 class='white-text'>Bucho de Bode</h3>
                <p class='grey-text text-lighten-4'>Restaurante</p>
              </div>
              <div class='col l4 offset-l2 s12'>
                <h5 class='white-text'>Siga-nos:</h5>
                <ul>
                  <li><a class='grey-text text-lighten-3' href=''>Instagram</a></li>
                  <li><a class='grey-text text-lighten-3' href=''>Twitter</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class='footer-copyright'>
            <div class='container'>
            © 2020 Copyright Bucho de Bode site.
            </div>
          </div>
        </footer>

<script  src='js/materialize.js'></script>
  <!-- É obrigatório chamar o script depois do código -->
  <script>

    $(document).ready(function(){
    $('.sidenav').sidenav();
  });

  </script>
  <script>
        $(document).ready(function(){
    $('.parallax').parallax();
  });
  </script>

  <script>
    
  <script>
      $('.dropdown-trigger').dropdown();
  </script>

  </script>


";
?>
